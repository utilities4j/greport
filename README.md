# GReport (em construção...)

Uma biblioteca Java poderosa e flexível para geração de relatórios em PDF sem Jasper/iReport, oferecendo recursos avançados de formatação e layout.

## Índice
- [Instalação](#instalação)
- [Funcionalidades](#funcionalidades)
- [Guia de Uso](#guia-de-uso)
  - [Conceitos Básicos](#conceitos-básicos)
  - [Criando Relatórios](#criando-relatórios)
  - [Trabalhando com Grids](#trabalhando-com-grids)
  - [Formatação Avançada](#formatação-avançada)
  - [Cabeçalhos e Rodapés](#cabeçalhos-e-rodapés)
  - [Operações em Colunas](#operações-em-colunas)
- [Exemplos Práticos](#exemplos-práticos)
- [Referência API](#referência-api)
- [Solução de Problemas](#solução-de-problemas)

## Instalação

Adicione a dependência ao seu arquivo `pom.xml`:

    </dependencies>
        <dependency>
            <groupId>org.greport</groupId>
            <artifactId>greport</artifactId>
            <version>1.1.0</version>
            <type>jar</type>
        </dependency>
    </dependencies>
    <repositories>
        <repository>
            <id>gitlab-maven</id>
            <url>https://gitlab.com/api/v4/groups/13274432/-/packages/maven</url>
        </repository>
    </repositories>

## Funcionalidades

### Recursos Principais
- 📄 Geração de relatórios em PDF
- 📊 Múltiplas grids/tabelas por página
- 🎨 Formatação rica de texto e células
- 📏 Layout flexível e responsivo
- 🖼️ Suporte a imagens e marcas d'água
- 📝 Conteúdo HTML nas células
- 🔢 Operações matemáticas em colunas

### Recursos de Formatação
- Estilos de texto (negrito, itálico, sublinhado)
- Cores personalizadas (texto e fundo)
- Diversos tipos de bordas
- Alinhamento preciso (horizontal e vertical)
- Margens e padding configuráveis
- Múltiplas famílias de fontes

## Guia de Uso

### Conceitos Básicos

1. **Inicializando um Relatório**

~~~java
Report report = new Report();
report.setTitle("Meu Primeiro Relatório")
.setPageSize(PageSize.A4)
.setOrientation(Orientation.PORTRAIT)
.setMargins(20, 20, 20, 20);
~~~

2. **Configuração Básica de Grid**

~~~java:
Grid grid = report.addGrid()
.addColumn("Nome")
.addColumn("Idade")
.addColumn("Cidade");
grid.addRow()
.addValue("João Silva")
.addValue(30)
.addValue("São Paulo");
~~~

### Trabalhando com Grids

1. **Configurando Colunas**

~~~java
grid.addColumn("Valor")
.setWidth(100) // Largura fixa
.setAlignment(HAlignment.RIGHT) // Alinhamento
.setFormat("#,##0.00") // Formato numérico
.setFooterOperation(Operation.SUM); // Operação de rodapé
~~~

2. **Estilos de Células**

~~~java
grid.addRow()
.addValue("Texto Importante")
.setFontStyle(FontStyle.BOLD)
.setBackgroundColor(Color.LIGHT_GRAY)
.setBorder(Borders.SOLID);
~~~

### Formatação Avançada

1. **Mesclando Células**

~~~java
grid.addRow()
.addValue("Total")
.setColSpan(3)
.setAlignment(HAlignment.CENTER);
~~~

2. **Usando HTML**

~~~java
grid.addRow()
.addHtmlValue("<b>Texto em Negrito</b> e <i>Itálico</i>");
~~~

### Cabeçalhos e Rodapés

1. **Cabeçalho de Página**

~~~java
report.addHeaderGrid()
.addColumn()
.addValue("RELATÓRIO DE VENDAS")
.setAlignment(HAlignment.CENTER)
.setFontSize(14);
~~~

2. **Rodapé com Paginação**

~~~java
report.addFooterGrid()
.addColumn()
.addValue("Página ${pageNumber} de ${totalPages}")
.setAlignment(HAlignment.RIGHT);
~~~

### Operações em Colunas

~~~java
grid.addColumn("Valor")
.setFooterOperation(Operation.SUM) // Soma
.setFooterFormat("#,##0.00"); // Formato
grid.addColumn("Quantidade")
.setFooterOperation(Operation.AVG) // Média
.setFooterFormat("#,##0"); // Formato inteiro
~~~

## Exemplos Práticos

### Relatório de Vendas

~~~java
Report report = new Report("Relatório de Vendas");
// Configuração do cabeçalho
Grid header = report.addHeaderGrid();
header.addColumn()
.addValue("RELATÓRIO DE VENDAS MENSAL")
.setFontSize(16)
.setAlignment(HAlignment.CENTER);
// Grid principal
Grid vendas = report.addGrid()
.addColumn("Data")
.addColumn("Produto")
.addColumn("Quantidade")
.addColumn("Valor Unitário")
.addColumn("Total");
// Adicionando dados
vendas.addRow()
.addValue(new Date())
.addValue("Produto A")
.addValue(10)
.addValue(50.00)
.addValue(500.00);
// Configurando totalizadores
vendas.getColumn("Total")
.setFooterOperation(Operation.SUM)
.setFooterFormat("#,##0.00");
// Gerando o PDF
Util.showReport(report);
~~~

## Referência API

### Classes Principais
- `Report`: Classe principal para criação de relatórios
- `Grid`: Representa uma tabela no relatório
- `Column`: Define uma coluna em uma grid
- `Row`: Representa uma linha de dados

### Enums Importantes
- `HAlignment`: LEFT, CENTER, RIGHT, JUSTIFIED
- `VAlignment`: TOP, MIDDLE, BOTTOM
- `Borders`: NONE, SOLID, DOTTED, DASHED
- `FontStyle`: NORMAL, BOLD, ITALIC, BOLD_ITALIC
- `Operation`: SUM, AVG, MIN, MAX, COUNT

## Solução de Problemas

### Problemas Comuns

1. **Texto Truncado**
   - Verifique a largura da coluna
   - Considere usar `setWordWrap(true)`
   - Ajuste o tamanho da fonte

2. **Formatação Incorreta**
   - Verifique o formato definido para a coluna
   - Certifique-se de que o tipo de dado está correto

3. **Desempenho**
   - Use `setLazy(true)` para grandes conjuntos de dados
   - Evite HTML desnecessário nas células
   - Otimize o número de linhas por página

### Dicas de Otimização

1. **Memória**
   - Libere recursos após gerar o relatório
   - Use streams para grandes conjuntos de dados
   - Evite carregar muitas imagens

2. **Performance**
   - Pré-calcule valores quando possível
   - Use formatação simples quando apropriado
   - Minimize o uso de fontes personalizadas

## Suporte

Para suporte técnico ou dúvidas:
- Abra uma issue no GitLab
- Consulte a documentação completa
- Entre em contato com o desenvolvedor

## Licença

Este projeto está licenciado sob os termos da licença [inserir tipo de licença].

## Autor

Desenvolvido por Marcius Brandão

---

**Nota**: Esta documentação está em constante evolução. Contribuições e sugestões são bem-vindas!

